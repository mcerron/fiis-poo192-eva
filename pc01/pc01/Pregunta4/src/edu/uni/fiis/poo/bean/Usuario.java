package edu.uni.fiis.poo.bean;

public class Usuario {

        public Integer codigo;
        private String nombre;
        private String apellido;
        private String correoElectronico;
        private Integer amonestaciones;
        private boolean esVip;

        public static final Integer MAX_AMONESTACIONES = 10;


        public Usuario(Integer codigo){
            this.codigo = codigo;
        }

        //*Me parece que va mejor siendo privada//*

        private String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    //*Me parece que va mejor siendo privada//*
        private String getApellido() {
            return apellido;
        }
    //*Me parece que va mejor siendo privada por seguridad de los datos//*
    private Integer getAmonestaciones() {
            return amonestaciones;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public void agregarAmonestacion(){
            amonestaciones++;
        }

        public void mostrarEstadoUsuario(Usuario usuario){
            if(usuario.getAmonestaciones() > MAX_AMONESTACIONES){
                System.out.println("Usuario suspendido");
            }else{
                System.out.println("Usuario habilitado");
            }
        }

        public void promover(){
            this.esVip = true;
        }

        public static void mostrarDatos(Usuario usuario){
            System.out.println("Datos del Usuario: ");
            System.out.println("Nombre: " + usuario.getNombre());
            System.out.println("Apellido: " + usuario.getApellido());
        }

    }
}
