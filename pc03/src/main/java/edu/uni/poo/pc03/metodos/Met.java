package edu.uni.poo.pc03.metodos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Met {
    public static void aumentarSalario(String employee_id){
        Connection conn=null;
        PreparedStatement st=null;
        ResultSet rs = null;
        try{
            Class.forName("org.postgresql.Driver");
            String url="jdbc:postgresql://localhost:5432/postgres";
            conn= DriverManager.getConnection(url);
            String sql="UPDATE manager_id SET salary = salary + 0.15*salary WHERE employee_id=?";
            st=conn.prepareStatement(sql);
            st.setString(1,employee_id);
            st.executeUpdate();


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Met p = new Met();
        p.aumentarSalario("01");

    }
}


