package edu.uni.poo.pc03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pc03Application {

	public static void main(String[] args) {
		SpringApplication.run(Pc03Application.class, args);
	}

}
